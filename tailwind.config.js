const colors = require("tailwindcss/colors");

module.exports = {
  purge: ["./src/**/*.{js,jsx,ts,tsx}", "./gatsby-browser.js"],
  darkMode: "class", // or 'media' or 'class'
  theme: {
    extend: {
      typography: (theme) => {
        const h = { color: theme("colors.blue.eyes") };
        const h2 = { color: theme("colors.green.from-eyes") }
        return {
          DEFAULT: {
            css: {
              color: theme("colors.mine.text"),
              h1: h,
              h3: h,
              h6: h,
              h2,
              h4: h2,
              a: { color: theme('colors.pink.accent') }
            },
          },
        };
      },
      colors: {
        brown: {
          coat: "#C79B6E",
          "coat-dark": "#8A6035",
        },
        blue: {
          eyes: "#739AC3",
        },
        green: {
          "from-eyes": "#8DC373"
        },
        pink: {
          accent: "#ED91B0"
        },
        mine: {
          text: colors.blue[300],
          dark: "#86416F",
        },
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [require("@tailwindcss/typography")],
};

import React from "react";
import Layout from "../components/Layout";
import { PageProps, graphql, Link } from "gatsby";
import Card from "../components/Card";

interface Edge {
  node: {
    fields: {
      slug: string;
    };
    frontmatter: {
      title: string;
    };
    headings: {
      value: string;
    };
    excerpt: string;
  };
}

interface Query {
  allMarkdownRemark: {
    edges: Edge[];
  };
}

export default function Index({ data }: PageProps & { data: Query }) {
  return (
    <Layout>
      <div className="flex flex-col gap-5 py-5 px-10 items-center">
        <Card>
          <div className="prose mx-auto py-10">
            <h1>So, this is a personal blog.</h1>
            <p>
              It should be fairly obvious, I&rsquo;m not experienced in web
              development. Yes, I wrote the blog markup too. Feel free to look
              around, remember I'm only human, and{" "}
              <a href="https://twitter.com/rhg135">here is my twitter</a>
            </p>
          </div>
        </Card>
        <Card>
          <p className="text-center text-2xl">Posts</p>
          <hr className="my-5" />
        <div className="grid grid-cols-3 lg:grid-cols-4 gap-4 text-center">
          {data.allMarkdownRemark.edges.map((edge) => (
            <Card>
              <Link to={edge.node.fields.slug} className="text-pink-accent">
                {edge.node.frontmatter.title}
              </Link>
              <div dangerouslySetInnerHTML={{ __html: edge.node.excerpt }} className="overflow-x-hidden"></div>
            </Card>
          ))}
        </div>
        </Card>
      </div>
    </Layout>
  );
}

export const query = graphql`
  fragment ExcerptFragment on MarkdownRemark {
    excerpt(format: HTML, pruneLength: 200, truncate: true)
  }
  {
    allMarkdownRemark(
      sort: { fields: frontmatter___modified, order: DESC }
      filter: { frontmatter: { title: { ne: "" }, public: { eq: true } } }
    ) {
      edges {
        node {
          ...ExcerptFragment
          fields {
            slug
          }
          frontmatter {
            title
          }
        }
      }
    }
  }
`;

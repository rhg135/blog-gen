import NavBar from "./NavBar";
import React from "react";

export default function Layout({ children }) {
  return (
    <div className="min-h-screen dark:bg-brown-coat-dark dark:text-mine-text ">
      <NavBar />
      {children}
    </div>
  );
}

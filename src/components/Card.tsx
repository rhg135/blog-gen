import React from 'react'

export default function Card({ children }) {
    return (
        <div className="shadow p-10">
            { children }
        </div>
    )
}
import { Link } from 'gatsby'
import React from 'react'

export interface Props {
    title?: string
}

export default function NavBar(props: Props) {
    return (
        <div className="grid grid-cols-3 gap-1 px-1 items-center pb-3 justify-items-start mb-3 w-full shadow-lg">
            <Link className="text-5xl" to="/">tehblog</Link>
            {'title' in props &&
                <span className="text-6xl font-extralight justify-self-center">{props.title}</span>}
            <span className="text-4xl font-light justify-self-end col-start-3">A blog about everything and nothing</span>
        </div>
    )
}
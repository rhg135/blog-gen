import React from "react";
import Layout from "../components/Layout";
import Card from "../components/Card";
import { graphql, Link } from "gatsby";

export interface OtherNode {
  node: {
    fields: {
      slug: string;
    };
    frontmatter: {
      title: string;
      modified: string;
    };
    excerpt: string;
  };
}

const quarters = {
  "1": "Early in",
  "2": "Summer of",
  "3": "Late summer of",
  "4": "Fall",
};

function quarterYearToDesc(s: string) {
  const [quarter, year] = s.split(":", 2);
  return `${quarters[parseInt(quarter)]} '${year}`;
}

export default function Story({ data }) {
  const post = data.markdownRemark;
  const links = data.allMarkdownRemark.edges.map((el: OtherNode) => {
    return (
      <Card>
        <Link to={el.node.fields.slug} className="text-pink-accent">
          {el.node.frontmatter.title}{" "}
          <small>{quarterYearToDesc(el.node.frontmatter.modified)}</small>
        </Link>
        <div className="prose prose-sm" dangerouslySetInnerHTML={{ __html: el.node.excerpt }} />
      </Card>
    );
  });
  const created = post.frontmatter.created;
  const modified = post.frontmatter.modified;
  return (
    <Layout>
      <div className="flex flex-col items-center px-10 py-5 gap-5">
        <Card>
          <div className="prose prose-xl">
            <h1>{post.frontmatter.title}</h1>
            <p>Created on: {created}</p>
            {modified && created != modified && <p>Updated on: {modified}</p>}
            <hr className="py-5" />
          </div>
          <article
            dangerouslySetInnerHTML={{ __html: post.html }}
            className="prose prose-xl"
          ></article>
        </Card>
        <Card>
          <p className="text-center text-xl">Other Works</p>
          <hr className="my-5" />
          <div className="grid grid-cols-3 lg:grid-cols-4 gap-4 text-center text-lg">
            {links}
          </div>
        </Card>
      </div>
    </Layout>
  );
}

export const query = graphql`
  query(
    $slug: String!
    $format: String = "MMM Do, YYYY @ hh:mm A"
    $f2: String = "Q:YY"
  ) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
        created(formatString: $format)
        modified(formatString: $format)
      }
    }
    allMarkdownRemark(
      filter: {
        fields: { slug: { ne: $slug } }
        frontmatter: { title: { ne: "" }, public: { eq: true } }
      }
      sort: { fields: frontmatter___modified, order: DESC }
    ) {
      edges {
        node {
          ...ExcerptFragment
          fields {
            slug
          }
          frontmatter {
            title
            modified(formatString: $f2)
          }
        }
      }
    }
  }
`;

const { createFilePath } = require(`gatsby-source-filesystem`)
const path = require(`path`)
const moment = require('moment')

exports.createSchemaCustomization = ({ actions, schema }) => {
  const { createTypes } = actions
  const typeDefs = [
    "type MarkdownRemark implements Node { frontmatter: Frontmatter }",
    schema.buildObjectType({
      name: "Frontmatter",
      fields: {
        public: {
          type: "Boolean",
          resolve(src) {
            return src['private'] !== true
          }
        },
        modified: {
          type: "Date",
          extensions: {
            dateformat: {}
          },
          resolve(source) {
            if ('modified' in source) return moment(source.modified, false)
          }
        },
      },
    }),
  ]
  createTypes(typeDefs)
}

exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions
  // Ensures we are processing only markdown files
  if (node.internal.type === "MarkdownRemark") {
    // if (node)
    const relativeFilePath = createFilePath({
      node,
      getNode,
      trailingSlash: false,
    })

    // Creates new query'able field with name of 'slug'
    createNodeField({
      node,
      name: "slug",
      value: relativeFilePath
    })
  }
}

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions
  const result = await graphql(`
    query {
      allMarkdownRemark(filter: {frontmatter: {title: {ne: ""}, public: {eq: true}}}) {
        edges {
          node {
            fields {
              slug
            }
          }
        }
      }
    }
  `)
  result.data.allMarkdownRemark.edges.forEach(({ node }) => {
    createPage({
      path: node.fields.slug,
      component: path.resolve(`./src/templates/story.tsx`),
      context: {
        // Data passed to context is available
        // in page queries as GraphQL variables.
        slug: node.fields.slug,
      },
    })
  })
}
module.exports = {
  siteMetadata: {
    title: "teh blog",
  },
  plugins: [
    "gatsby-plugin-sharp",
    {
      resolve: "gatsby-transformer-remark",
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              // It's important to specify the maxWidth (in pixels) of
              // the content container as this plugin uses this as the
              // base for generating different widths of each image.
              maxWidth: 700, // not sure what but on my monitor it is about 700
            },
          },
          {
            resolve: "gatsby-remark-smartypants",
            options: {
              dashes: "oldschool"
            }
          }
        ]
      }
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "pages",
        path: "./src/pages/",
      },
      __key: "pages",
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "journal",
        path: process.env.JOURNAL_ROOT || "../journal",
        ignore: ["**/gen", "**/.git"]
      },
    },
    "gatsby-plugin-postcss"
  ],
};
